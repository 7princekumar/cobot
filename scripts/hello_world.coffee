# Description:
#   Invite newcomers to GitHub organization when they say Hello World.
#
# Dependencies:
#   None
#
# Configuration:
#   HUBOT_GITHUB_OAUTH - The GitHub OAuth token
#   GITHUB_NEWCOMERS_TEAM - The team ID of the newcomers team.
#
# Commands:
#   Hello World - Get invited to the coala organization.
#
# Author:
#   Vamshi Krishna(@Vamshi99)

gh_token = process.env.HUBOT_GITHUB_OAUTH
gh_user = process.env.GITHUB_NEWCOMERS_TEAM

module.exports = (robot) ->
  robot.hear /hello\s*,?\s*world/i, (msg) ->
    user = msg.message.user.login

    robot.http("https://api.github.com/teams/#{gh_user}/memberships/#{user}")
      .header('Authorization', "token #{gh_token}")
      .put() (err, res, tbody) ->
        if err
          msg.send "Oh no! Error inviting the user: #{err}"
          return
        if res.statusCode isnt 200
          msg.send "Invite error: HTTP #{res.statusCode} :worried:"
          return
        msg.send "Welcome @#{user}! :tada:\n\nTo get started, please follow our [newcomers guide](https://coala.io/newcomer). Most issues will be explained there and in linked pages - it will save you a lot of time, just read it. *Really.*\n\n*Do not take an issue if you don't understand it on your own.* Especially if you are new you have to be aware that getting started with an open source community is not trivial: you will have to work hard and most likely become a better coder than you are now just as we all did.\n\nDon't get us wrong: we are *very* glad to have you with us on this journey into open source! We will also be there for you at all times to help you with actual problems. :)"
